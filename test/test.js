let PluginBase = require("../src/plugin");
let PluginManager = require("../src/manager");

class TestPlugin extends PluginBase {
    constructor() {
        super("TestPlugin", "1.0.0");
    }

    onLoad() {
        console.log("[PLUGIN] TestPlugin loaded");
    }

    onUnload() {
        console.log("[PLUGIN] TestPlugin unloaded");
    }
}

let testPlugin = new TestPlugin();
testPlugin.registerHandler("test", function (param1, param2) {
    console.log("[PLUGIN] TestPlugin test event with parameter (" + param1 + ", " + param2 + ")");
})

let pluginManager = new PluginManager();
pluginManager.loadPluginsByObject(testPlugin);

console.log("triggering test event");
pluginManager.callEvent("test", 114514, "This is a test");

pluginManager.unloadPlugins();
