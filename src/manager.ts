import { PluginFindInfo } from "./loader";
import PluginBase from "./plugin";

const { loadPlugins, findPlugins, unloadPlugins } = require("./loader");

class PluginManager {
    plugins: Array<PluginBase>;

    constructor() {
        this.plugins = [];
    }

    loadPluginsByInfo(info: PluginFindInfo) {
        var newPlugins = findPlugins(info);
        loadPlugins(newPlugins);
        this.plugins = this.plugins.concat(newPlugins);
    }

    loadPluginsByObject(...plugins: Array<PluginBase>) {
        loadPlugins(plugins);
        this.plugins = this.plugins.concat(plugins);
    }

    unloadPlugins() {
        unloadPlugins(this.plugins);
        this.plugins = [];
    }

    callEvent(eventName: string, ...params: Array<any>) {
        this.plugins.forEach(function (plugin) {
            plugin.on(eventName, ...params);
        })
    }
}

export default PluginManager;