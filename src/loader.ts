import PluginBase from "./plugin";
import * as fs from 'fs';
import * as path from 'path';

export type PluginFindInfo = {
    paths: Array<string>,
    basePath: string,
};

export function findPlugins(info: PluginFindInfo): Array<PluginBase> {
    let paths = info.paths;
    let plugins = [];
    for (var i = 0; i < paths.length; i++) {
        let pluginPath = paths[i];
        // check if path is relative or absolute
        if (path.resolve(pluginPath) === pluginPath) {
            // absolute path, don't add base path
        } else {
            // relative path
            pluginPath = path.resolve(info.basePath, pluginPath);
        }
        // check if path exists
        if (!fs.existsSync(pluginPath)) {
            console.log("Plugin path does not exist: " + pluginPath);
            continue;
        }
        // plugin exports a class that extends PluginBase
        (async () => {
            let pluginModule = await import(pluginPath);
            return new pluginModule.default();
        })().then((plugin: PluginBase) => {
            let hasDuplicated = plugins.find(function (p) {
                return p.getName() === plugin.getName();
            }) || plugins.push(plugin);
            if (hasDuplicated == true) {
                console.error(`Plugin ${plugin.getName()} from ${path} is duplicated.`);
            }
        }).catch(e => {
            console.error("Error finding plugin " + path + ": " + e.stack);
        })
    }
    return plugins;
}

export function loadPlugins(plugins: Array<PluginBase>) {
    plugins.forEach((plugin) => {
        loadPlugin(plugin);
    })
}

export function loadPlugin(plugin: PluginBase) {
    console.log("Loading plugin " + plugin.getName() + " v" + plugin.getVersion());
    try {
        plugin.onLoad();
        plugin.loaded = true;
    } catch (e) {
        console.error("Error loading plugin " + plugin.getName() + ": " + e.stack);
        return false
    }
    return true
}

export function unloadPlugins(plugins: Array<PluginBase>) {
    plugins.forEach((plugin) => {
        unloadPlugin(plugin);
    });
}

export function unloadPlugin(plugin: PluginBase) {
    console.log("Unloading plugin " + plugin.getName() + " v" + plugin.getVersion());
    try {
        plugin.onUnload();
        plugin.loaded = false;
    } catch (e) {
        console.error("Error unloading plugin " + plugin.getName() + ": " + e.stack);
        return false
    }
    return true
}

module.exports = {
    findPlugins,
    loadPlugins,
    loadPlugin,
    unloadPlugins,
    unloadPlugin,
};