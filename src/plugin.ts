class PluginBase {
    name: string;
    version: string;
    loaded: boolean;
    handlers: Map<string, Function>;

    constructor(name: string, version: string) {
        this.name = name;
        this.version = version;
        this.loaded = false;
        this.handlers = new Map();
    }

    onLoad() {
        // Override this method to do something when the plugin is loaded
    }

    onUnload() {
        // Override this method to do something when the plugin is unloaded
    }

    on(event: string, ...args: Array<any>) {
        if (this.handlers[event]) {
            this.handlers[event](...args);
        }
    }

    registerHandler(event: string, handler: Function) {
        this.handlers[event] = handler;
    }
    
    getName() {
        return this.name;
    }
    
    getVersion() {
        return this.version;
    }
}

export default PluginBase;