# Yuri-API
This is the plugin api for [Yuri](https://bitbucket.org/hosh1no/yuri/) proxy client.

## Examples
Have a look in [test](https://bitbucket.org/hosh1no/yuri-api/src/main/test/)

## Event list
We are working for the plugin api, and the list is in [Yuri](https://bitbucket.org/hosh1no/yuri/) repo.