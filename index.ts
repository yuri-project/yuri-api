import * as loader from "./src/loader";
import PluginManager from "./src/manager";
import PluginBase from "./src/plugin";

module.exports = {
    loader,
    PluginBase,
    PluginManager,
};